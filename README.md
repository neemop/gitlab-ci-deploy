# gitlab-ci-deploy
Docker image to use in GitLab CI/CD pipelines. Includes ansible, role stack-deploy

# Usage:

Use the image raarts/gitlab-ci-deploy on Docker Hub as the image in .gitlab-ci.yml file. 

See the documentation for [gitlab-ci-utils](https://github.com/raarts/gitlab-ci-utils) for further info.

